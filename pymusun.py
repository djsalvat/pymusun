import ROOT
from time import time

def mr_legend(hist_list,text_list,size=(0.6,0.6,0.95,0.9)):
        '''Take a list of histograms and list of collated legend labels
        (strings). A legend is generated and returned. The optional
        size setting expects a tuple (xlo,ylo,xhi,yhi).
        '''
        leg = ROOT.TLegend(*size)
        for hist,text in zip(hist_list,text_list):
                leg.AddEntry(hist,text)
        return leg

def Integralv(self,lo,hi):
        '''adds a method to histogram classes to get integral
        based on the axis value instead of the bin numbers.
        '''
        return self.Integral(self.FindBin(lo),self.FindBin(hi))
ROOT.TH1.Integralv = Integralv

def AddSmart(self,hist,c):
        '''Save as the usual "Add" method, but instead it returns
        a new histogram as a result, with an auto-generated name
        '''
        newhist = self.Clone(self.GetName()+'_plus_'+str(c)+'*'+hist.GetName())
        newhist.Add(hist,c)
        return newhist
ROOT.TH1.AddSmart = AddSmart

def make_fitfunc(name,h,lo,hi):
        '''generates an exponential plus background fit function with the given name.
        the histogram is passed in order to set guesses for the parameter values.
        '''
        f = ROOT.TF1(name,'[1]*TBlindingOffset::UnblindThisValueMuMinus([0])'
                          '*exp(-TBlindingOffset::UnblindThisValueMuMinus([0])*x)+[2]',lo,hi)
        f.SetParameter(0,1.0/2200.0)
        f.SetParameter(1,h.Integralv(160,24000.0))
        f.SetParameter(2,h.GetBinContent(h.FindBin(23500)))
        return f

def make_residual(hist,func,restype='normed'):
        '''takes a histogram and a tf1 and returns a residual histogram.
        restype can be 'normed' (default) which plots (y-f(x))/sigma,
        'unnormed' which plots (y-f(x)), or 'relative' which plots (y-f(x))/f(x).
        '''
        hist_ = hist.Clone(hist.GetName()+'_res')
        for b in range(1,hist_.GetNbinsX()+1):
                content = hist_.GetBinContent(b)
                funcval = func.Eval(hist_.GetBinCenter(b))
                err = hist_.GetBinError(b)
                if restype=='normed':
                        if err > 0.0:
                                hist_.SetBinContent(b,(content-funcval)/err)
                                hist_.SetBinError(b,1.0)
                        else:
                                hist_.SetBinContent(b,0.0)
                                hist_.SetBinError(b,0.0)
                elif restype=='unnormed':
                        hist_.SetBinContent(b,content-funcval)
                        hist_.SetBinError(b,err)
                elif restype=='relative':
                        hist_.SetBinContent(b,(content-funcval)/funcval)
                        hist_.SetBinError(b,err/funcval)
        return hist_

def get_slice(h,v1,v2):
        '''takes a 2D histogram and returns
        1D projection along X axis with
        Y limited between v1 and v2.
        '''
        h.GetYaxis().SetRangeUser(v1,v2)
        return h.ProjectionX()

def start_time_scan(h,start_times,end_time):
        '''takes a histogram, a list of start times,
        and a stop time, and performs a start time scan.
        returns tuple of 5 objects: a tgraph
        of the scanned fitted R, tgraph of fitted N,
        tgraph of fitted B, tgraph of the chisquares,
        and a residual histogram of the fit for the first
        start time in the scan.
        '''
        gR = ROOT.TGraphErrors(len(start_times))
        gR.SetTitle(h.GetName()+' R')
        gN = ROOT.TGraphErrors(len(start_times))
        gN.SetTitle(h.GetName()+' N')
        gB = ROOT.TGraphErrors(len(start_times))
        gB.SetTitle(h.GetName()+' B')
        gChi = ROOT.TGraphErrors(len(start_times))
        gChi.SetTitle(h.GetName()+' Chi2')

        for j,start_time in enumerate(start_times):
                f = make_fitfunc(h.GetName()+'start_scan',h,start_time,end_time)
                result = h.Fit(f,'NRQLLS','',start_time,end_time)
                if int(result):
                        print 'trying again'
                        result = h.Fit(f,'NRQLLS','',start_time,end_time)
                if j==0:
                        res_hist = make_residual(h,f)

                R = (f.GetParameter(0)-1.0/2.1969811e3)*1e9
                Rsigma = f.GetParError(0)*1e9
                gR.SetPoint(j,start_time,R)
                gR.SetPointError(j,0.0,Rsigma)

                N = f.GetParameter(1)/40.0
                Nsigma = f.GetParError(1)/40.0
                gN.SetPoint(j,start_time,N)
                gN.SetPointError(j,0.0,Nsigma)

                BN = f.GetParameter(2)/N
                BNsigma = f.GetParError(2)/N
                gB.SetPoint(j,start_time,BN)
                gB.SetPointError(j,0.0,BNsigma)

                Chi2 = f.GetChisquare()/f.GetNDF()
                Chi2sigma = ROOT.TMath.Sqrt(2.0/f.GetNDF())
                gChi.SetPoint(j,start_time,Chi2)
                gChi.SetPointError(j,0.0,Chi2sigma)
        return gR,gN,gB,gChi,res_hist

def lifetime_scan(h,scan_variable,limits,start_time,end_time):
        '''similar to start_time_scan, but takes a 2D histogram,
        a string label representing the variable k to be scanned,
        a list of limit pairs [(k_lo1,k_hi1),(k_lo2,k_hi2),...]
        which determines the slices along the Y axis to take and fit,
        and the start and stop time for the fits.
        returns the same set of objects as the start time scan function.
        '''
        gR = ROOT.TGraphErrors(len(limits))
        gR.SetTitle(h.GetName()+' R')
        gN = ROOT.TGraphErrors(len(limits))
        gN.SetTitle(h.GetName()+' N')
        gB = ROOT.TGraphErrors(len(limits))
        gB.SetTitle(h.GetName()+' B')
        gChi = ROOT.TGraphErrors(len(limits))
        gChi.SetTitle(h.GetName()+' Chi2')

        for j,(scan_var_lo,scan_var_hi) in enumerate(limits):
                h_ = get_slice(h,scan_var_lo,scan_var_hi)
                f = make_fitfunc(h_.GetName()+'scan%s%d'%(scan_variable,j),h_,start_time,end_time)
                result = h_.Fit(f,'NRQLLS','',start_time,end_time)
                if int(result):
                        print 'trying again'
                        result = h_.Fit(f,'NRQLLS','',start_time,end_time)
                if j==1:
                        res_hist = make_residual(h_,f)


                R = (f.GetParameter(0)-1.0/2.1969811e3)*1e9
                Rsigma = f.GetParError(0)*1e9
                gR.SetPoint(j,scan_var_lo,R)
                gR.SetPointError(j,0.0,Rsigma)

                N = f.GetParameter(1)/40.0
                Nsigma = f.GetParError(1)/40.0
                gN.SetPoint(j,scan_var_lo,N)
                gN.SetPointError(j,0.0,Nsigma)

                BN = f.GetParameter(2)/N
                BNsigma = f.GetParError(2)/N
                gB.SetPoint(j,scan_var_lo,BN)
                gB.SetPointError(j,0.0,BNsigma)

                Chi2 = f.GetChisquare()/f.GetNDF()
                Chi2sigma = ROOT.TMath.Sqrt(2.0/f.GetNDF())
                gChi.SetPoint(j,scan_var_lo,Chi2)
                gChi.SetPointError(j,0.0,Chi2sigma)
        return gR,gN,gB,gChi,res_hist

def get_lifetime_histogram(f,condition='',
			     eldef='CathOR',
			     scan='VsGond',
                             limits=None):
        '''gets a histogram from the file f, projected
        along the X (time) axis. the keyword
        arguments are used to construct the histogram name
        based on the naming conventions in HistBuilder.
        The optional limits permit retrieving a slice
        along the Y axis.
        '''
        hist_name = 'hLifetime' + condition + eldef + scan

        h2 = getattr(f,hist_name)
        if limits is not None:
                lo, hi = limits
                h2.GetYaxis().SetRangeUser(lo,hi)
        return h2.ProjectionX()

def get_lifetime_histogram2d(f,condition='',
			     eldef='CathOR',
			     scan='VsGond',
                             limits=None):
        '''Same as the get_lifetime_histogram function,
        but does not project along the time axis.
        '''
        hist_name = 'hLifetime' + condition + eldef + scan

        h2 = getattr(f,hist_name)
        return h2

def make_fake_lthist(h,R=1.0/2200.0):
        '''generate and return a pseudodata histogram
        with exponential plus background with total
        N and background B based on the provided
        histogram h. The decay rate can be provided.
        '''
        B = h.GetBinContent(h.FindBin(23500.0))
        N = (h.GetBinContent(h.FindBin(40.0))-B)/(40.0*R)
        hnew = h.Clone(h.GetName()+'_fake')
        rnd = ROOT.TRandom2(int(time()))
        for b in range(1,hnew.GetNbinsX()+1):
                hnew.SetBinContent(b,rnd.Poisson(int(40.0*R*N*ROOT.TMath.Exp(-R*hnew.GetBinCenter(b))+B)))
        return hnew
